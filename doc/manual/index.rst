.. OVITO User Manual master file

.. _ovito_user_manual:

*****************
OVITO User Manual
*****************

.. sidebar:: Quick links

  * :ref:`particles.modifiers`
  * :ref:`file_formats.input`
  * :ref:`file_formats.output`
  * :ref:`Python API <scripting_manual>`

.. toctree::
  :maxdepth: 1
  :includehidden:

  introduction
  What's new <new_features>

.. toctree::
  :maxdepth: 2
  :includehidden:

  installation
  usage/index
  tutorials/index
  advanced_topics/index
  reference/index
  ovito_pro
  development/index
  licenses/index

..
  * :ref:`about_ovito`
  * :ref:`new_features`
  * :ref:`installation`

    * :ref:`installation.requirements`
    * :ref:`installation.instructions`
    * :ref:`installation.remote`
    * :ref:`installation.python`
    * :ref:`installation.troubleshooting`

  * :ref:`basic_usage`

    * :ref:`usage.import`
    * :ref:`usage.viewports`
    * :ref:`usage.data_model`
    * :ref:`usage.modification_pipeline`
    * :ref:`usage.rendering`
    * :ref:`usage.miscellaneous`
    * :ref:`usage.export`

  * :ref:`tutorials`

    * :ref:`tutorials.marker_particles`
    * :ref:`tutorials.turntable_animation`

  * :ref:`advanced_topics`

    * :ref:`howto.transparent_particles`
    * :ref:`howto.aspherical_particles`
    * :ref:`usage.animation`
    * :ref:`clone_pipeline`
    * :ref:`usage.import.remote`
    * :ref:`custom_initial_session_state`
    * :ref:`viewport_layouts`
    * :ref:`python_code_generation`
    * :ref:`howto.scale_bar`

  * :ref:`reference`

    * :ref:`reference.pipelines`
    * :ref:`reference.viewports`
    * :ref:`rendering`
    * :ref:`data_inspector`
    * :ref:`application_settings`

  * :ref:`credits.ovito_pro`

    * :ref:`license-management`