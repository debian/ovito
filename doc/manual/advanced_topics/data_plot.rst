.. _howto.data_plot:

Adding a data plot to a viewport |ovito-pro|
============================================

https://github.com/ovito-org/DataTablePlotOverlay

.. image:: /images/viewport_layers/viewport_layer_data_plot.*
  :width: 55%
  :align: right

A contributed Python extension is available for OVITO Pro, which allows adding a data plot
to a viewport. The plot is displayed as an overlay on top of the rendered image and can be used
incorporate additional, dynamically computed information by OVITO in a visualization.

.. seealso:: `OVITO Extensions Directory <https://www.ovito.org/extensions/>`__