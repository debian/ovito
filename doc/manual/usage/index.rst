.. _basic_usage:

===========
Basic usage
===========

The following sections will introduce you to the basic usage of the OVITO desktop application.

.. toctree::
  :maxdepth: 1
  :includehidden:

  import
  viewports
  data_model
  pipeline
  rendering
  miscellaneous
  export
