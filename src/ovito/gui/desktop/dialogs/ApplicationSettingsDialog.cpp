////////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2023 OVITO GmbH, Germany
//
//  This file is part of OVITO (Open Visualization Tool).
//
//  OVITO is free software; you can redistribute it and/or modify it either under the
//  terms of the GNU General Public License version 3 as published by the Free Software
//  Foundation (the "GPL") or, at your option, under the terms of the MIT License.
//  If you do not alter this notice, a recipient may use your version of this
//  file under either the GPL or the MIT License.
//
//  You should have received a copy of the GPL along with this program in a
//  file LICENSE.GPL.txt.  You should have received a copy of the MIT License along
//  with this program in a file LICENSE.MIT.txt
//
//  This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
//  either express or implied. See the GPL or the MIT License for the specific language
//  governing rights and limitations.
//
////////////////////////////////////////////////////////////////////////////////////////

#include <ovito/gui/desktop/GUI.h>
#include <ovito/gui/desktop/mainwin/MainWindow.h>
#include <ovito/gui/base/actions/ActionManager.h>
#include <ovito/core/app/PluginManager.h>
#include "ApplicationSettingsDialog.h"

namespace Ovito {

IMPLEMENT_OVITO_CLASS(ApplicationSettingsDialogPage);

/******************************************************************************
* The constructor of the settings dialog class.
******************************************************************************/
ApplicationSettingsDialog::ApplicationSettingsDialog(MainWindow& mainWindow, OvitoClassPtr startPage) : QDialog(&mainWindow), _mainWindow(mainWindow)
{
    setWindowTitle(tr("Application Settings"));

    QVBoxLayout* layout1 = new QVBoxLayout(this);

    // Create dialog contents.
    _tabWidget = new QTabWidget(this);
    layout1->addWidget(_tabWidget);

    // Instantiate all ApplicationSettingsDialogPage derived classes.
    for(OvitoClassPtr clazz : PluginManager::instance().listClasses(ApplicationSettingsDialogPage::OOClass())) {
        try {
            OORef<ApplicationSettingsDialogPage> page = static_object_cast<ApplicationSettingsDialogPage>(clazz->createInstance());
            page->_settingsDialog = this;
            _pages.push_back(std::move(page));
        }
        catch(const Exception& ex) {
            mainWindow.reportError(ex);
        }
    }

    // Sort pages.
    std::sort(_pages.begin(), _pages.end(), [](const auto& page1, const auto& page2) { return page1->pageSortingKey() < page2->pageSortingKey(); });

    // Show pages in dialog.
    int defaultPage = 0;
    for(const auto& page : _pages) {
        if(startPage && startPage->isMember(page)) defaultPage = _tabWidget->count();
        page->insertSettingsDialogPage(_tabWidget);
    }
    _tabWidget->setCurrentIndex(defaultPage);

    // Add a label that displays the location of the application settings store on the computer.
    QLabel* configLocationLabel = new QLabel();
    configLocationLabel->setText(tr("<p style=\"font-size: small; color: #686868;\">Program settings are stored in %1</p>").arg(QSettings().fileName()));
    configLocationLabel->setTextInteractionFlags(Qt::TextSelectableByMouse);
    layout1->addWidget(configLocationLabel);

    // Ok and Cancel buttons
    QDialogButtonBox* buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel | QDialogButtonBox::Help, Qt::Horizontal, this);
    connect(buttonBox, &QDialogButtonBox::accepted, this, &ApplicationSettingsDialog::onOk);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &ApplicationSettingsDialog::reject);
    connect(buttonBox, &QDialogButtonBox::helpRequested, this, &ApplicationSettingsDialog::onHelp);
    connect(this, &QDialog::rejected, this, &ApplicationSettingsDialog::onCancel);
    layout1->addWidget(buttonBox);
}

/******************************************************************************
* This is called when the user has pressed the OK button of the settings dialog.
* Validates and saves all settings made by the user and closes the dialog box.
******************************************************************************/
void ApplicationSettingsDialog::onOk()
{
    try {
        setFocus(); // Remove focus from child widgets to commit newly entered values in text widgets etc.

        // Let all pages validate the changes the user made to the settings.
        for(const OORef<ApplicationSettingsDialogPage>& page : _pages) {
            if(!page->validateValues(_tabWidget)) {
                return;
            }
        }

        // Let all pages save their settings.
        for(const OORef<ApplicationSettingsDialogPage>& page : _pages) {
            page->saveValues(_tabWidget);
        }

        // Close dialog box.
        accept();
    }
    catch(const Exception& ex) {
        mainWindow().reportError(ex, this);
    }
}

/******************************************************************************
* This is called when the user closes the dialog box using the Cancel button.
******************************************************************************/
void ApplicationSettingsDialog::onCancel()
{
    try {
        setFocus(); // Remove focus from child widgets to commit newly entered values in text widgets etc.

        // Let all pages restore their settings to the old values.
        for(const OORef<ApplicationSettingsDialogPage>& page : _pages)
            page->restoreValues(_tabWidget);
    }
    catch(const Exception& ex) {
        mainWindow().reportError(ex, this);
    }
}

/******************************************************************************
* This is called when the user has pressed the help button of the settings dialog.
******************************************************************************/
void ApplicationSettingsDialog::onHelp()
{
    mainWindow().actionManager()->openHelpTopic(QStringLiteral("manual:application_settings"));
}

/******************************************************************************
* Returns the main window hosting this settings page.
******************************************************************************/
MainWindow& ApplicationSettingsDialogPage::mainWindow() const
{
    return settingsDialog()->mainWindow();
}

}   // End of namespace
